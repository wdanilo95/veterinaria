<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial', function (Blueprint $table) {
            $table->bigIncrements('id_historial');
            $table->unsignedBigInteger('id_mascota');
            $table->foreign('id_mascota')->references('id_mascota')->on('mascota');
            $table->date('fecha_consulta');
            $table->string('motivo_atencion');
            $table->text('diagnostico');
            $table->text('tratamiento');
            $table->text('observacion');
            $table->date('fecha_proxconsulta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial');
    }
}
