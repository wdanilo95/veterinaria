<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMascotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mascota', function (Blueprint $table) {
            $table->bigIncrements('id_mascota');
            $table->integer('cod_mascota')->unique();
            $table->string('nombre_mascota');
            $table->date('fecha_nacimiento');
            $table->unsignedBigInteger('id_Sexo');
            $table->foreign('id_Sexo')->references('id_Sexo')->on('sexo');
            $table->decimal('peso');
            $table->integer('edad');
            $table->string('color');
            $table->unsignedBigInteger('id_tipo_mascota');
            $table->foreign('id_tipo_mascota')->references('id_tipo_mascota')->on('tipo_mascota');
            $table->string('raza');
            $table->unsignedBigInteger('id_dueno');
            $table->foreign('id_dueno')->references('id_dueno')->on('dueno');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mascota');
    }
}
