<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
             'name' => 'William Quiano',
           'email' => 'wdaniloq@gmail.com',
          'username' => 'wdanilo95',
           'email_verified_at' => now(),
            'is_admin' =>true,
           'password' => Hash::make('12345678'),
           'created_at' => now(),
           'updated_at' => now(),

           ]);

        DB::table('users')->insert([
             'name' => 'Danilo Quiano',
           'email' => 'pumahacker95@hotmail.com',
          'username' => 'wdanilo',
           'email_verified_at' => now(),
            'is_admin' =>false,
           'password' => Hash::make('12345678'),
           'created_at' => now(),
           'updated_at' => now(),

           ]);


    }
}
