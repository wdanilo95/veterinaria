<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InventarioSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TipoMascotaSeeder::class);
        $this->call(DuenoSeeder::class);
        $this->call(SexoSeeder::class);
        $this->call(MascotaSeeder::class);
        $this->call(HistorialSeeder::class);


    }
}
