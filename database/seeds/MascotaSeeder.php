<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MascotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('mascota')->insert([

        'cod_mascota' => '1',
        'nombre_mascota' => 'Franchesca',
        'fecha_nacimiento' => '2015-08-30',
        'id_Sexo' => 2,
        'peso' => '100.02',
        'edad' => '12',
        'color' => 'cafe con negro',
        'id_tipo_mascota' => 1,
        'raza' => 'Pug',
        'id_dueno' => 1,
        'created_at' => now(),
        'updated_at' => now(),

          ]);

          DB::table('mascota')->insert([

            'cod_mascota' => '2',
            'nombre_mascota' => 'Fiona',
            'fecha_nacimiento' => '2011-09-28',
            'id_Sexo' => 2,
            'peso' => '200',
            'edad' => '16',
            'color' => 'cafe',
            'id_tipo_mascota' => 1,
            'raza' => 'Shar pei',
            'id_dueno' => 2,
            'created_at' => now(),
            'updated_at' => now(),

              ]);


    }
}
