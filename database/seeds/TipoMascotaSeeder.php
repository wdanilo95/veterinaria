<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoMascotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_mascota')->insert([
            'mascota' => 'Perro',
          ]);

          DB::table('tipo_mascota')->insert([
            'mascota' => 'Gato',
          ]);


    }
}
