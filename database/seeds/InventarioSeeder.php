<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InventarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventarios')->insert([
            'nombre_producto' => 'Shampoo',
                'descripcion' => 'rico',
                'marca' => 'headshoulder',
               'precio' => '10',
               'slug' => 1,
              'stock' => 10,

          ]);

        DB::table('inventarios')->insert([
            'nombre_producto' => 'Peine',
                'descripcion' => 'rico',
                'marca' => 'Ivonne',
               'precio' => '5',
               'slug' => 2,
              'stock' => 20,

          ]);

        DB::table('inventarios')->insert([
            'nombre_producto' => 'Jabon',
                'descripcion' => 'rico',
                'marca' => 'Dove',
               'precio' => '2.50',
               'slug' => 3,
              'stock' => 50,

          ]);


    }
}
