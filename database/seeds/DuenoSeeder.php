<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DuenoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dueno')->insert([
            'DUI' => '02548892-8',
            'nombres' => 'Victor Manuel',
            'apellidos' => 'Mazariego Regalado',
            'direccion' => 'Res.Altavista psj 5 pol O Tonacatepeque',
            'telefono' => '7894-2368',
            'correo' => 'vic.colo@gmail.com',
            'created_at' => now(),
            'updated_at' => now(),

          ]);

          DB::table('dueno')->insert([
            'DUI' => '04584892-8',
            'nombres' => 'Miguel Angel',
            'apellidos' => 'Henrique Erroa',
            'direccion' => 'Res.Altavista psj 9 block A cs 45 Ilopango',
            'telefono' => '7987-2545',
            'correo' => 'siman222@gmail.com',
            'created_at' => now(),
            'updated_at' => now(),

          ]);
    }
}
