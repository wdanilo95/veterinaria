<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('historial')->insert([

            'id_mascota' => 1,
            'fecha_consulta' => '2020-03-05 ',
            'motivo_atencion' => 'Dolor de oidos',
            'diagnostico' => 'Infeccion',
            'tratamiento' => 'Gotas de Argan',
            'observacion' => 'Se puede observer una infeccion leve con pus',
            'fecha_proxconsulta' => '2020-07-08 ',
            'created_at' => now(),
            'updated_at' => now(),

              ]);

              DB::table('historial')->insert([

                'id_mascota' => 2,
                'fecha_consulta' => '2020-08-08 ',
                'motivo_atencion' => 'Dolor de pansa',
                'diagnostico' => 'Infeccion',
                'tratamiento' => 'Pastillas de infeccion',
                'observacion' => 'Se puede persivir aire',
                'fecha_proxconsulta' => '2020-12-05 ',
                'created_at' => now(),
                'updated_at' => now(),

                  ]);
    }
}
