<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sexo')->insert([
            'Sexo' => 'Macho',
            'created_at' => now(),
            'updated_at' => now(),

          ]);

          DB::table('sexo')->insert([
            'Sexo' => 'Hembra',
            'created_at' => now(),
            'updated_at' => now(),

          ]);
    }
}
