$(document).ready(function(){

	function inventario()
	{
		$.ajax({

			url:'{{route("getInventario")}}',

			success: function(datos)
			{
				var tabla='';
				//Tambien una variable para recorre el for segun la cantidad de datos que recibimos
				var i;
				//Con la estructura for recorreomos los datos recibidos
				for(i=0; i<datos.length; i++){
					//la variable tabla llenamos y formamos la estructura de la tabla
					tabla+=
					'<tr class="bg-dark">'+
					'<td>'+datos[i].nombre_producto+'</td>'+
					'<td>'+datos[i].descripcion+'</td>'+
					'<td>'+datos[i].marca+'</td>'+
					'<td>'+datos[i].precio+'</td>'+
					'<td>'+datos[i].stock+'</td>'+
					'<td>'+
					//Creamos un grupo de botones de 2 botones: 1 para eliminar (borrar) y otro para edita (item-edit)
					'<div class="btn-group" role="group" aria-label="Basic example">'+
					'<a href="javascript:;" class ="btn btn-danger btn-sm borrar1"  data="'+datos[i].slug+'">Eliminar</a>'+
					'<a href="javascript:;" class="btn btn-info btn-sm item-edit1"  data="'+datos[i].slug+'">Editar</a>'+
					'</div> '+
					'</td>'+

					'</tr>';
				}
				//en la vista principal en el id "tabla_empresa" le entregaremos la variable que contiene el cuerpo de la tabla
				$('#inventario').html(tabla);
			}
		});
	};

	$('#inventario').on('click', '.borrar1', function(){
		var id = $(this).attr('data');
		$('#modalBorrar').modal('show');
		$('#BorrarI').unbind().click(function(){


			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type : 'POST',
				url : '{{ route("eliminar") }}',
				data : {'slug':id},

				success: function(datos){
					$('#modalBorrar').modal('hide');
					if (datos.msj=='del') {
						inventario();
						toast('Registro Eliminado','success');

					}else{
						toast('Error al Eliminar Registro','error');
					}
				}
			});
		});
	});


});
