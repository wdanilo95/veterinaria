<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $fillable = ['nombre_producto','descripcion','marca','precio','slug','stock'];
}
