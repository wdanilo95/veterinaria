<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mascota;

class MascotaController extends Controller
{
    public function index()
    {
        $query = Mascota::select('tipo_mascota.mascota' ,'mascota.cod_mascota','nombre_mascota','fecha_nacimiento','sexo.Sexo','mascota.peso','mascota.edad','mascota.color','mascota.raza','dueno.nombres','dueno.apellidos')
                        ->join('dueno','dueno.id_dueno','=','mascota.id_dueno')
                        ->join('tipo_mascota','tipo_mascota.id_tipo_mascota','=', 'mascota.id_tipo_mascota')
                        ->join('sexo','sexo.id_Sexo','=','mascota.id_Sexo')
                        ->get();
       return view('dashboard.mascotas')->with('query',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInventario()
    {
        $query = Mascota::get();
        return Response()->json($query);
    }
}
