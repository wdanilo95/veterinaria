<?php

namespace App\Http\Controllers;

use App\Inventario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use  Yajra\DataTables\Facades\DataTables;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Inventario::get();


       return view('dashboard.inventario')->with('query' , $query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInventario()
    {
        $query = Inventario::get();
        return Response()->json($query);
    }

    public function removeInventario($id){

        DB::table('inventarios')->where('slug', $id)->delete();
        $array = array('msj' => "del");
        return response()->json($array);

    }

    public function addInventario(Request $req){
      DB::table('inventarios')->insert([
          'nombre_producto' => $req->producto,
          'descripcion' => $req->descripcion,
          'marca' => $req->marca,
          'precio' => $req->precio,
          'slug' => $req->slug,
          'stock' => $req->stock
      ]);

      $array = array('msj' => "add");

      return response()->json($array);
    }

    
}
