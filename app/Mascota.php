<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
   protected $table='mascota';
   protected $fillable = ['cod_mascota','nombre_mascota','fecha_nacimiento','id_sexo','peso','edad','color','id_tipo_mascota','raza','id_dueno','created_at', 'updated_at'];

}
