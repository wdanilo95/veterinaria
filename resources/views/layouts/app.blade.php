<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Custom fonts for this template -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/scrollnav.css')}}">
    <link rel="stylesheet" href="{{asset('css/alertify.min.css')}}">

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('js/jquery.easing.min.js')}}"></script>

    <style media="screen">
    @media (max-width: @screen-xs-min) {
    .modal-xs { width: @modal-sm; }
    }
    </style>

</head>
<body id="page-top">
    <div id="app">
      <header>
        <nav class="navbar menu-fixed navbar-expand-md navbar-light bg-white shadow-sm ">
            <div class="container">
               @if(Route::has('home'))
               <a class="navbar-brand" href="{{ route('home') }}">

                @else
                <a class="navbar-brand" href="{{ route('inicio') }}">
                    @endif

                    {{ config('app.name') }}
                </a>
                <button id="sidebarToggleTop" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest



                        @if(Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">{{ __('Iniciar Sesion') }}</a>
                        </li>


                        @endif
                        @else
                        <li class="nav-item dropdown" >
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>

    </nav>
   </header>


    <main class="py-4">

      <!---alertas-->
      @include('sweetalert::alert')

        @yield('content')



    </main>

</div>


<script src="{{asset('js/scrollnav.js')}}"></script>
<script src="{{asset('js/alertify.min.js')}}"></script>
</body>
</html>
