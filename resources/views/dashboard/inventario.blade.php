@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.css') }}">
<!-- Page level plugins -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/datatables-demo.js') }}"></script>



@if(Auth::user()->is_admin == 1)
<!-- Page Wrapper -->
<div id="wrapper" >


  @include('layouts.nav')
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">



      <!-- Begin Page Content -->
      <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800" align="center">Registro de Inventario</h1>
        <p class="mb-4"></p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary" align="center" >Productos</h4>
          </div>
          <div class="card-body">
            <button class="btn btn-success btn-block" id="btnNVinv"><i class="fa fa-plus-circle"></i> Nuevo Producto</button><hr>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>

                  <tr>

                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Marca</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>

                  </tr>
                </thead>

                <tbody id="inventario">
                  @forelse($query as $f)
                  <tr>
                    <td>{{$f->slug}}</td>
                    <td>{{$f->nombre_producto}}</td>
                    <td>{{$f->descripcion}}</td>
                    <td>{{$f->marca}}</td>
                    <td> ${{number_format($f->precio,2)}} </td>
                    <td>{{$f->stock}}</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="javascript:;" class="btn btn-info btn-md item-edit1" data="{{$f->slug}}"><i class="fa fa-edit" ></i></a>
                        <div class="btn-group" role="group" arial-label="Basic example">
                          <a href="javascript:;" class ="btn btn-danger btn-md borrar1" data="{{$f->slug}}"><i class="fa fa-trash-alt" ></i></a>
                        </div>
                      </td>
                    </tr>


                    @empty
                    <tr>
                      <td style="display: none"></td>
                      <td style="display: none"></td>
                      <td style="display: none"></td>
                      <td colspan="8" align="center"><p>No existen Producto, Por favor ingrese una nueva!</p></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  @elseif(Auth::user()->is_admin == 0)

  <!-- Page Wrapper -->
<div id="wrapper">


  @include('layouts.nav')
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">



      <!-- Begin Page Content -->
      <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800" align="center">Registro de Inventario</h1>
        <p class="mb-4"></p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary" align="center" >Productos</h4>
          </div>
          <div class="card-body">
            <button class="btn btn-success btn-block"><i class="fa fa-plus-circle"></i> Nuevo Producto</button><hr>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>

                  <tr>

                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Marca</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>

                  </tr>
                </thead>
                <tfoot>
                  <tr>

                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Marca</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>

                  </tr>
                </tfoot>
                <tbody id="inventario">
                  @forelse($query as $f)
                  <tr>

                    <td>{{$f->nombre_producto}}</td>
                    <td>{{$f->descripcion}}</td>
                    <td>{{$f->marca}}</td>
                    <td> ${{number_format($f->precio,2)}} </td>
                    <td>{{$f->stock}}</td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="javascript:;" class="btn btn-info btn-sm item-edit1" data="{{$f->slug}}"><i class="fa fa-edit" ></i> Editar</a>
                        <div class="btn-group" role="group" arial-label="Basic example">

                        </div>
                      </td>
                    </tr>


                    @empty
                    <tr>
                      <td style="display: none"></td>
                      <td style="display: none"></td>
                      <td style="display: none"></td>
                      <td colspan="4" align="center"><p>No existen Empresa, Por favor ingrese una nueva!</p></td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  @endif

  <!---  Modal para Agregar Inventario --->
  <div class="modal" id="modalAddInv" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="FormInv"  method="post">
          @csrf
          <input type="hidden" id="id" value="0">
          <div class="form-group row">
              <label for="slug" class="col-md-4 col-form-label text-md-right">{{ __('Codigo Unico') }}</label>

              <div class="col-md-6">
                  <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug') }}" placeholder="Codigo del Producto" required autocomplete="slug" autofocus style="border-radius:20px">

                  @error('slug')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label for="producto" class="col-md-4 col-form-label text-md-right">{{ __('Producto') }}</label>

              <div class="col-md-6">
                  <input id="producto" type="text" class="form-control @error('producto') is-invalid @enderror" name="producto" value="{{ old('producto') }}" placeholder="Nombre del Producto" required autocomplete="producto" autofocus style="border-radius:20px">

                  @error('producto')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

              <div class="col-md-6">
                  <textarea id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ old('descripcion') }}" placeholder="Descripcion" required autocomplete="descripcion" autofocus style="border-radius:20px">
                  </textarea>
                  @error('descripcion')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label for="marca" class="col-md-4 col-form-label text-md-right">{{ __('Marca') }}</label>

              <div class="col-md-6">
                  <input id="marca" type="text" class="form-control @error('marca') is-invalid @enderror" name="marca" value="{{ old('marca') }}" placeholder="Marca" required autocomplete="marca" autofocus style="border-radius:20px">

                  @error('marca')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>


          <div class="form-group row">
              <label for="precio" class="col-md-4 col-form-label text-md-right">{{ __('Precio $') }}</label>

              <div class="col-md-6">
                  <input id="precio" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{ old('precio') }}" placeholder="Precio" required autocomplete="precio" autofocus style="border-radius:20px">

                  @error('precio')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>

          <div class="form-group row">
              <label for="stock" class="col-md-4 col-form-label text-md-right">{{ __('Cantidad') }}</label>

              <div class="col-md-6">
                  <input id="stock" type="text" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock') }}" placeholder="Cantidad Disponible" required autocomplete="stock" autofocus style="border-radius:20px">

                  @error('stock')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnAdd">Agregar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>


  <!---  Modal para Borrar Inventario --->
  <div class="modal" id="modalBorrarInv" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Eliminar Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Desea Eliminar El Registro ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="BorrarI">Eliminar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>




  <script src="{{asset('js/sb-admin-2.min.js')}}"></script>
  <script>
  $(document).ready(function(){

    function inventario()
    {
      $.ajax({

        url:'{{route("getInventario")}}',

        success: function(datos)
        {
          var tabla='';
          //Tambien una variable para recorre el for segun la cantidad de datos que recibimos
          var i;
          //Con la estructura for recorreomos los datos recibidos
          for(i=0; i<datos.length; i++){
            //la variable tabla llenamos y formamos la estructura de la tabla
            tabla+=
            '<tr >'+
            '<td>'+datos[i].slug+'</td>'+
            '<td>'+datos[i].nombre_producto+'</td>'+
            '<td>'+datos[i].descripcion+'</td>'+
            '<td>'+datos[i].marca+'</td>'+
            '<td>'+datos[i].precio+'</td>'+
            '<td>'+datos[i].stock+'</td>'+
            '<td>'+
            //Creamos un grupo de botones de 2 botones: 1 para eliminar (borrar) y otro para edita (item-edit)
            '<div class="btn-group" role="group" aria-label="Basic example">'+
            '<a href="javascript:;" class="btn btn-info btn-md item-edit1"  data="'+datos[i].slug+'"><i class="fa fa-edit" ></i></a>'+
            '<div class="btn-group" role="group" aria-label="Basic example">'+
            '<a href="javascript:;" class ="btn btn-danger btn-md borrar1"  data="'+datos[i].slug+'"><i class="fa fa-trash-alt" ></i></a>'+
            '</div> '+
            '</div> '+
            '</td>'+

            '</tr>';
          }
          //en la vista principal en el id "tabla_empresa" le entregaremos la variable que contiene el cuerpo de la tabla
          $('#inventario').html(tabla);
        }
      });
    };

    $('#inventario').on('click', '.borrar1', function(){
      var id = $(this).attr('data');

      $('#modalBorrarInv').modal('show');
      $('#BorrarI').unbind().click(function(){


        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
          type : 'DELETE',
          url : "/eliminar/"+id,
          data : {'slug':id},

          success: function(datos){
            $('#modalBorrarInv').modal('hide');
            if (datos.msj=='del') {
              alertify.alert("Eliminada Con Exito!", function(){
                  alertify.message('OK');
                });
              inventario();
            }else{
              alertify.error('Error al Eliminar Producto');
            }
          }
        });
      });
    });

    $('#btnNVinv').click(function(){
      $('#modalAddInv').modal('show');
      $('#modalAddInv').find('.modal-title').text('Nuevo Producto');
      $('#FormInv').attr('action','{{route("addinv")}}');
    });

    $('#btnAdd').click(function(){
      var url = $('#FormInv').attr('action');
      var data = $('#FormInv').serialize();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        type : 'POST',
        url : url,
        data : data,

        success: function(datos){
          $('#modalAddInv').modal('hide');
          if (datos.msj == 'add') {
            alertify.alert("Registro Agregado!", function(){
                alertify.message('OK');
              });
          }else{
            alertify.error('Error al Agregar Producto');
          }
          inventario();
        }
      });
      $("#btnNVinv").on("click",function(){
            $('#FormInv').trigger("reset");
});
    });

    

  });

  </script>

  @endsection
