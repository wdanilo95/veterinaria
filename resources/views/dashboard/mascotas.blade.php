@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{asset('css/material-design-iconic-font.min.css')}}">
<link rel="stylesheet" href="{{asset('css/main.min.css')}}">




<!-- Page level plugins -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>

<!-- Page level custom scripts -->
<script src="{{ asset('js/datatables-demo.js') }}"></script>



@if(Auth::user()->is_admin == 1)
<!-- Page Wrapper -->
<div id="wrapper">


  @include('layouts.nav')
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">



      <!-- Begin Page Content -->
      <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800" align="center">Registro de Mascotas</h1>
        <p class="mb-4"></p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary" align="center" >Mascotas</h4>
          </div>
          <div class="card-body">
            <hr>
            <a href="" class="btn btn-success btn-block">Nuevo Mascota</a>

        <div class="table-responsive" style="padding-top: 25px">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>

              <tr class="text-center">

                <th>Codigo</th>
                <th>Nombre</th>
                <th>Fecha Nacimiento</th>
                <th>Peso</th>
                <th>Edad</th>
                <th>Color</th>
                <th>Tipo de Mascota</th>
                <th>Raza</th>
                <th>Dueño</th>
                <th>Fecha Registro Creado</th>
                <th>Fecha Registro Actualizado</th>
                <th>Acciones</th>



              </tr>
            </thead>
            <tbody id="mascota">
              @forelse($query as $m)
              <tr class="text-center">


                <td>{{$m->cod_mascota}}</td>
                <td>{{$m->nombre_mascota}}</td>
                <td>{{$m->fecha_nacimiento}}</td>
                <td>{{$m->Sexo}}</td>
                <td> {{$m->peso}} </td>
                <td>{{$m->edad}}</td>
                <td>{{$m->color}}</td>
                <td>{{$m->color}}</td>
                <td>{{$m->mascota}}</td>
                <td>{{$m->raza}}</td>
                <td>{{$m->nombres.' '.$m->apellidos}}</td>




                <td>
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="javascript:;" class="btn btn-info btn-md item-edit1" data="{{$m->cod_mascota}}"><i class="fa fa-edit" ></i></a>
                    <div class="btn-group" role="group" arial-label="Basic example">
                      <a href="javascript:;" class ="btn btn-danger btn-md borrar1" data="{{$m->cod_mascota}}"><i class="fa fa-trash-alt" ></i></a>
                    </div>
                  </td> 
                </tr>


                @empty
                <tr>
                  <td style="display: none"></td>
                  <td style="display: none"></td>
                  <td style="display: none"></td>
                  <td colspan="4" align="center"><p>No existen Empresa, Por favor ingrese una nueva!</p></td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <!-- Footer -->
  <footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>Copyright &copy; Your Website 2020</span>
      </div>
    </div>
  </footer>
  <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>

@elseif(Auth::user()->is_admin == 0)

<!-- Page Wrapper -->
<div id="wrapper">


  @include('layouts.nav')
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">



      <!-- Begin Page Content -->
      <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800" align="center">Registro de Mascotas</h1>
        <p class="mb-4"></p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h4 class="m-0 font-weight-bold text-primary" align="center" >Mascotas</h4>
          </div>
          <div class="card-body">
            <hr>

            
            <form class="wizard-container" method="POST"  action="#" id="js-wizard-form">
              <h1 class="text-center" id="text"></h1>
              <script>
               $(document).ready(function(){
                $('#text').html('Dueño');
                $("#btn").click(function(){
                  $('#text').html('Mascota');
                });
                $("#back").click(function(){
                  $('#text').html('Dueño');
                });
              });
            </script>
            <div class="progress" id="js-progress">                       
              <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                <span class="progress-val">70%</span>
              </div>
            </div>
            <ul class="nav nav-tab">
              <li class="active">
                <a href="#tab1" data-toggle="tab">1</a>
              </li>
              <li>
                <a href="#tab2" data-toggle="tab">1</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active text-center" id="tab1">
               <div class="form-group row">
                <label for="Dui" class="col-md-4 col-form-label text-md-right">{{ __('DUI') }}</label>

                <div class="col-md-6">
                  <input id="Dui" type="text" class="form-control @error('Dui') is-invalid @enderror" name="Dui" value="{{ old('Dui') }}" maxlength="10" placeholder="DUI" required autocomplete="Dui" autofocus style="border-radius:20px">

                  @error('Dui')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

                <div class="col-md-6">
                  <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" placeholder="Nombres Dueño" required autocomplete="nombre" autofocus style="border-radius:20px">

                  @error('nombre')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="apellidos" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                <div class="col-md-6">
                  <input id="apellidos" type="text" class="form-control @error('apellidos') is-invalid @enderror" name="apellidos" value="{{ old('apellidos') }}" placeholder="Apellidos Dueño" required autocomplete="apellidos" autofocus style="border-radius:20px">

                  @error('apellidos')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                <div class="col-md-6">
                  <textarea id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" placeholder="Escribe una Direccion....." required autocomplete="direccion" autofocus style="border-radius:20px">
                    </textarea>

                  @error('direccion')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Telefono') }}</label>

                <div class="col-md-6">
                  <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" placeholder="(503)xxxx-xxxx" required autocomplete="telefono" autofocus style="border-radius:20px">

                  @error('telefono')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="correo" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>

                <div class="col-md-6">
                  <input id="correo" type="text" class="form-control @error('correo') is-invalid @enderror" name="correo" value="{{ old('correo') }}" placeholder="ejemplo@ejemplo.com" required autocomplete="correo" autofocus style="border-radius:20px">

                  @error('correo')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              <div class="btn-next-con">
                <a class="btn-next" id="btn" href="#">Next</a>
              </div>
            </div>
            <div class="tab-pane" id="tab2">
              <div class="form-group row">
                <label for="numero" class="col-md-4 col-form-label text-md-right">{{ __('N°') }}</label>

                <div class="col-md-6">
                  <input id="numero" type="text" class="form-control @error('numero') is-invalid @enderror" name="numero" value="{{ old('numero') }}" placeholder="Numero de Mascota" required autocomplete="numero" autofocus style="border-radius:20px" maxlength="10">

                  @error('numero')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="nombre_mascota" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Mascota') }}</label>

                <div class="col-md-6">
                  <input id="nombre_mascota" type="text" class="form-control @error('nombre_mascota') is-invalid @enderror" name="nombre_mascota" value="{{ old('nombre_mascota') }}" placeholder="Nombre" required autocomplete="nombre_mascota" autofocus style="border-radius:20px">

                  @error('nombre_mascota')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
               <div class="form-group row">
                <label for="fnacimiento" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Nacimiento') }}</label>

                <div class="col-md-6">
                  <input id="fnacimiento" type="date" class="form-control @error('fnacimiento') is-invalid @enderror" name="fnacimiento" value="{{ old('fnacimiento') }}" required autocomplete="fnacimiento" autofocus style="border-radius:20px">

                  @error('fnacimiento')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="sexo" class="col-md-4 col-form-label text-md-right">{{ __('Sexo') }}</label>

                <div class="col-md-6">
                  <select id="sexo" type="date" class="form-control @error('sexo') is-invalid @enderror" name="sexo" value="{{ old('sexo') }}" required autocomplete="sexo" autofocus style="border-radius:20px">
                      <option value="">--seleccione una opcion--</option>
                  </select>

                  @error('sexo')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="peso" class="col-md-4 col-form-label text-md-right">{{ __('Peso') }}</label>

                <div class="col-md-6">
                  <input id="peso" type="text" class="form-control @error('peso') is-invalid @enderror" name="peso" value="{{ old('peso') }}" placeholder="Libras" required autocomplete="peso" autofocus style="border-radius:20px">

                  @error('peso')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="edad" class="col-md-4 col-form-label text-md-right">{{ __('Edad') }}</label>

                <div class="col-md-6">
                  <input id="edad" type="text" class="form-control @error('edad') is-invalid @enderror" name="edad" value="{{ old('edad') }}" placeholder="Edad" maxlength="3" required autocomplete="edad" autofocus style="border-radius:20px">

                  @error('edad')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="color" class="col-md-4 col-form-label text-md-right">{{ __('Color') }}</label>

                <div class="col-md-6">
                  <input id="color" type="text" class="form-control @error('color') is-invalid @enderror" name="color" value="{{ old('color') }}" placeholder="Color" required autocomplete="edad" autofocus style="border-radius:20px">

                  @error('color')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="tipo_mascota" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de Mascota') }}</label>

                <div class="col-md-6">
                  <select id="tipo_mascota" type="date" class="form-control @error('tipo_mascota') is-invalid @enderror" name="tipo_mascota" value="{{ old('tipo_mascota') }}" required autocomplete="tipo_mascota" autofocus style="border-radius:20px">
                      <option value="">--seleccione una opcion--</option>
                  </select>

                  @error('tipo_mascota')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="raza" class="col-md-4 col-form-label text-md-right">{{ __('Raza') }}</label>

                <div class="col-md-6">
                  <input id="raza" type="text" class="form-control @error('raza') is-invalid @enderror" name="raza" value="{{ old('raza') }}" placeholder="Raza" required autocomplete="edad" autofocus style="border-radius:20px">

                  @error('color')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="dueño" class="col-md-4 col-form-label text-md-right">{{ __('Dueño') }}</label>

                <div class="col-md-6">
                  <select id="dueño" type="date" class="form-control @error('dueño') is-invalid @enderror" name="dueño" value="{{ old('dueño') }}" required autocomplete="dueño" autofocus style="border-radius:20px">
                      <option value="">--seleccione una opcion--</option>
                  </select>

                  @error('dueño')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
              <div class="btn-next-con">
                <a class="btn-back" href="#" id="back">back</a>
                <a class="btn-last" href="#">Submit</a>
              </div>
            </div>
            
          </div>
        </form>


        <div class="table-responsive" style="padding-top: 25px">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>

              <tr class="text-center">

                <th>Codigo</th>
                <th>Nombre</th>
                <th>Fecha Nacimiento</th>
                <th>Peso</th>
                <th>Edad</th>
                <th>Color</th>
                <th>Tipo de Mascota</th>
                <th>Raza</th>
                <th>Dueño</th>
                <th>Fecha Registro Creado</th>
                <th>Fecha Registro Actualizado</th>
                <th>Acciones</th>



              </tr>
            </thead>
            <tbody id="mascota">
              @forelse($query as $m)
              <tr class="text-center">


                <td>{{$m->cod_mascota}}</td>
                <td>{{$m->nombre_mascota}}</td>
                <td>{{$m->fecha_nacimiento}}</td>
                <td>{{$m->Sexo}}</td>
                <td> {{$m->peso}} </td>
                <td>{{$m->edad}}</td>
                <td>{{$m->color}}</td>
                <td>{{$m->color}}</td>
                <td>{{$m->mascota}}</td>
                <td>{{$m->raza}}</td>
                <td>{{$m->nombres.' '.$m->apellidos}}</td>




                <td>
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="javascript:;" class="btn btn-info btn-md item-edit1" data="{{$m->cod_mascota}}"><i class="fa fa-edit" ></i></a>
                    <div class="btn-group" role="group" arial-label="Basic example">
                      <a href="javascript:;" class ="btn btn-danger btn-md borrar1" data="{{$m->cod_mascota}}"><i class="fa fa-trash-alt" ></i></a>
                    </div>
                  </td> 
                </tr>


                @empty
                <tr>
                  <td style="display: none"></td>
                  <td style="display: none"></td>
                  <td style="display: none"></td>
                  <td colspan="4" align="center"><p>No existen Empresa, Por favor ingrese una nueva!</p></td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->

  <!-- Footer -->
  <footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>Copyright &copy; Your Website 2020</span>
      </div>
    </div>
  </footer>
  <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>


  @endif

  <script src="{{asset('js/sb-admin-2.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/jquery.bootstrap.wizard.min.js')}}"></script>
  <script src="{{asset('js/jquery.validate.min.js')}}"></script>
  <script src="{{asset('js/global.js')}}"></script>
  <script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
  <script>
    $(document).ready(function(){
  $('#Dui').inputmask("99999999-9");  //static mask
  $('#telefono').inputmask({"mask": "(503) 999-9999"}); //specifying options
  $("#correo").inputmask('email');
  $("#numero").inputmask("999999999");
  $("#peso").inputmask("999.99");
  $("#edad").inputmask("999");
  $(selector).inputmask("9-a{1,3}9{1,3}"); //mask with dynamic syntax
});
  </script>

  @endsection