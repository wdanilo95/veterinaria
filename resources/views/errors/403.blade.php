@extends('errors::illustrated-layout')

@section('title', __('Prohibido'))
@section('image')


<img src="{{ asset('img/Error.jpg') }}" alt="" style="width:120%; height:100%;padding-top:40px " >

@endsection
@section('code', '403')
@section('message', __($exception->getMessage() ?: 'Prohibido'))
