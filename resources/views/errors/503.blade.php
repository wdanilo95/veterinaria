@extends('errors::illustrated-layout')

@section('title', __('Servicio no Disponible'))
@section('image')


<img src="{{ asset('img/Error.jpg') }}" alt="" style="width:120%; height:100%;padding-top:40px " >

@endsection
@section('code', '503')
@section('message', __($exception->getMessage() ?: 'Servicio no Disponible'))
