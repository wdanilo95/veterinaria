@extends('errors::illustrated-layout')

@section('title', __('Demasiadas Solicitudes'))
@section('image')


<img src="{{ asset('img/Error.jpg') }}" alt="" style="width:120%; height:100%;padding-top:40px " >

@endsection
@section('code', '429')
@section('message', __('Demasiadas Solicitudes'))
