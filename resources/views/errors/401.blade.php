@extends('errors::illustrated-layout')

@section('title', __('No Autorizado'))
@section('image')


<img src="{{ asset('img/Error.jpg') }}" alt="" style="width:120%; height:100%;padding-top:40px " >

@endsection
@section('code', '401')
@section('message', __('No Autorizado'))


