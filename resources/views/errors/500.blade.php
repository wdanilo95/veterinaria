@extends('errors::illustrated-layout')

@section('title', __('Error del Servidor'))
@section('image')


<img src="{{ asset('img/Error.jpg') }}" alt="" style="width:120%; height:100%;padding-top:40px " >

@endsection
@section('code', '500')
@section('message', __('Error del Servidor'))
