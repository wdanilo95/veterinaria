@extends('errors::illustrated-layout')

@section('title', __('Pagina Expiro'))
@section('image')


<img src="{{ asset('img/Error.jpg') }}" alt="" style="width:120%; height:100%;padding-top:40px " >

@endsection
@section('code', '419')
@section('message', __('Pagina Expiro'))


