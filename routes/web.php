<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Vista inicial el login o inicio de sesion*/
Route::get('/', function () {
    return view('auth.login');
})->name('inicio');



/* rutas de Autenticacion para el registro, inicio de sesion etc.*/
Auth::routes();
/*vista home ya autentificado el usuario redirege a pagina principal de sistema*/
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/inventario', 'InventarioController@index')->name('inventario');
Route::get('/getInventario','InventarioController@getInventario')->name('getInventario');
Route::post('/agregar', 'InventarioController@addInventario')->name('addinv');
Route::delete('/eliminar/{slug}', 'InventarioController@removeInventario')->name('eliminar');

Route::get('/mascota', 'MascotaController@index')->name('mascota');
